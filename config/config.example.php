<?php

/**
 * All config parameters of the API are define here.
 * The file is automatically included in files that include vendor/autoload.php.
 * 
 * First, copy and paste this file. The copied file must be renamed 'config.php'.
 * Then, fill app information in the 'config.php' file.
 * 
 * 'config.example.php' must not contain sensitive data, as the project is
 * stored in a remote git repository.
 */

/**
 * General configuration
 */
$GLOBALS['ROOT_PATH'] = dirname(__DIR__);
$GLOBALS['PROJECT_NAME'] = '';

/**
 * Database configuration
 */
$GLOBALS['DB_HOST'] = '';
$GLOBALS['DB_USERNAME'] = '';
$GLOBALS['DB_PASSWORD'] = '';
$GLOBALS['DB_DBNAME'] = '';

/**
 * Slim Framework configuration
 */
$GLOBALS['displayErrorDetails'] = false; // should be false in production
