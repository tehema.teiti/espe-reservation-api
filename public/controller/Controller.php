<?php

namespace api\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * All controllers should extend this class.
 * This class provides function to construct response message.
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
abstract class Controller
{
    
    /** @var ContainerInterface */
    protected $container;
    
    public function __construct()
    {
    }
    
    /**
     * Construct a JSON response with a code and a message.
     * The message must be an array, then it will be transform as a JSON string
     * in this function.
     * 
     * @param ResponseInterface $response
     * @param int $code
     * @param array $message
     * @return
     */
    protected function jsonResponse(ResponseInterface $response, int $code, array $message)
    {
        $response = $response->withStatus($code)
            ->withHeader('Content-Type', 'application/json');

        $response->getBody()->write(json_encode($message));

        return $response;
    }
    
    /**
     * Print a message in the standard output to the client and
     * stop the script.
     * @param int $code
     * @param string $message
     */
    protected function exit(int $code, string $message)
    {
        http_response_code($code);
        echo $message;
        die();
    }
}
