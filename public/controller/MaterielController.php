<?php

namespace api\controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use src\model\MaterielModel;

/**
 * Description of MaterielController
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
class MaterielController extends Controller
{
    /** @var MaterielModel */
    private $materielModel;
    
    public function __construct() {
        $this->materielModel = new MaterielModel();
    }
    
    /**
     * Get all materials
     * 
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     */
    public function getAll(ServerRequestInterface $request, ResponseInterface $response, $args)
    {
        $materiels = $this->materielModel->getAll();
        
        $jsonResponse = $this->jsonResponse($response, 200, $materiels);
        return $jsonResponse;
    }
}
