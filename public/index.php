<?php

use api\API;

require dirname(__DIR__) . '/vendor/autoload.php';

$api = new API();
$api->run();
