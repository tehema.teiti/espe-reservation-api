<?php

namespace api;

use api\controller\MaterielController;
use Slim\App;
use Slim\Factory\AppFactory;

/**
 * Main class of the API.
 * Use Slim Framework as an HTTP router
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
class API {
    
    /** @var App */
    private $slimApp;
    
    public function run()
    {
        $this->slimApp = AppFactory::create();
        
        $this->slimApp->addRoutingMiddleware();
        $this->route();
        $this->slimApp->addErrorMiddleware($GLOBALS['SLIM_DISPLAY_ERROR_DETAILS'], true, true);
        
        $this->slimApp->run();
    }
    
    private function route() {
        $this->slimApp->get('/materiel', MaterielController::class . ':getAll');
        // $this->slimApp->post('/data', DataController::class . ':post');
//        $this->slimApp->get('/v1/materiel', function (ServerRequestInterface $request, ResponseInterface $response, $args) {
//            $response->getBody()->write("Hi");
//            return $response;
//        });
    }
    
}
