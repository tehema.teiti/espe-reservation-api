# API de réservation de matériel

## Présentation

L'API gère les réservations de matériels, principalement centré sur la gestion de prêts
de matériels numériques (Ipads, ordinateurs portables, tablettes, chargeurs, câbles...).

## License

Ce projet est sous licence [MIT](license.md).