<?php

namespace src\model;

/**
 * Description of MaterielModel
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
class MaterielModel extends Model
{

    public function getAll()
    {
        $sql = 'SELECT id, name '
                . 'FROM materiel';
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
}
