<?php

namespace src\model;

/**
 * Abstract class
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
abstract class Model
{
    /** @var \PDO */
    protected $db;
    
    public function __construct()
    {
        $this->db = DatabaseConnection::getInstance();
    }
    
    /**
     * Start a transaction.
     * Need to be commited in order to validate a set of SQL statements using
     * commit().
     */
    public function beginTransaction()
    {
        $this->db->beginTransaction();
    }
    
    public function rollBack()
    {
        $this->db->rollBack();
    }
    
    public function commit()
    {
        $this->db->commit();
    }
    
}
