<?php

namespace src\model;

/**
 * Singleton class for database connection.
 * Database parameters are stored in /config/config.php in
 * $GLOBALS variable.
 *
 * @author Tehema Teiti <tehema.teiti@gmail.com>
 */
class DatabaseConnection
{

    /** @var \PDO */
    private static $db;
    
    private function __construct() {}
    
    /**
     * Return a PDO instance of the database connection.
     * @return \PDO
     */
    public static function getInstance()
    {
        if (is_null(self::$db)) {
            self::$db = new \PDO('mysql:host=' . $GLOBALS['DB_HOST'] . ';dbname=' . $GLOBALS['DB_DBNAME'],
                    $GLOBALS['DB_USERNAME'], $GLOBALS['DB_PASSWORD']);
        }
        return self::$db;
    }
    
}
